FROM quay.io/ulrichschreiner/vscode-remote-base:latest

LABEL maintainer "ulrich.schreiner@gmail.com"

RUN    apt update       \
    && apt -y install   \
              nodejs    \
              npm       \
    && npm install -g imp-central-impt Builder

CMD ["bash"]
