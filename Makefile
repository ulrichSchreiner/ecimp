all: ecimp ;

kaniko:
	kaniko \
			--label org.opencontainers.image.title="$(TITLE)" \
			--label org.opencontainers.image.created="$(shell date  --rfc-3339=seconds)" \
			--label org.opencontainers.image.vcs-url="$(shell git remote get-url origin)" \
			--label org.opencontainers.image.revision="$(shell git rev-parse HEAD)" \
			--destination $(IMAGE) \

genericdocker:
	docker build \
			--label org.opencontainers.image.title="$(TITLE)" \
			--label org.opencontainers.image.created="$(shell date  --rfc-3339=seconds)" \
			--label org.opencontainers.image.vcs-url="$(shell git remote get-url origin)" \
			--label org.opencontainers.image.revision="$(shell git rev-parse HEAD)" \
			-f $(DOCKERFILE) \
			-t $(IMAGE) \
			.

ecimp:
	$(MAKE) genericdocker TITLE="ElectricImp Toolbox Image" DOCKERFILE=Dockerfile IMAGE=registry.gitlab.com/ulrichschreiner/ecimp